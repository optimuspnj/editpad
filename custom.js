$(document).ready(function () {
    $('[data-toggle="popover"]').popover({placement: "top"});
    $('#copy').popover().on('shown.bs.popover', function () {
        setTimeout(function () {
            $('#copy').popover('hide');
        }, 2000);
    });
    $('#clear').popover().on('shown.bs.popover', function () {
        setTimeout(function () {
            $('#clear').popover('hide');
        }, 2000);
    });
});

function copyText() {
    var copyText = $('#content');
    copyText.select();
    document.execCommand('copy');
}

function parsingData (contentText) {
    var fileName = prompt("Save As:", "Untitled.txt");
    if (fileName == "") {
        var fileName = prompt("You MUST enter a file name!\nSave As:", "Untitled.txt");
      } 
    else if (fileName == null) {
        //This prevent null saves
    }
    else {
        saveTextAsFile(contentText,fileName)
      }
}

function saveTextAsFile(textToWrite, fileNameToSaveAs) {
    var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'}); 
    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";
    if (window.webkitURL != null) {
        // Chrome allows the link to be clicked without actually adding it to the DOM.
        downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
    }
    else {
        // Firefox requires the link to be added to the DOM before it can be clicked.
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
        downloadLink.onclick = destroyClickedElement;
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);
    }
downloadLink.click();
}